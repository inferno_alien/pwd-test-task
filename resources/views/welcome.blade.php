<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Тестовое задание</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>

<?
use App\Models\Book;
use \App\Models\Author;
use App\Models\Publisher;

/* ----- Задание №1 ------ */
$author = Author::find(1);

$countBooks = $author->books->where('publisher_id', 1)->count();

/* ----- Задание №2 ------ */
$book = Book::find(4);

$authorsCount = $book->authors->count();
$profit = (new Publisher)->find($book->publisher_id)->author_profit;
$authorFee = $profit / $authorsCount * $book->copies_cnt;
?>
<body>
    <h2>Задание №1</h2>
    <h3>Кол-во книг автора с id=1, изданных издательством id=1</h3>
    {{$countBooks}}

    <h2>Задание №2</h2>
    <h3>Гонорар каждого соавтора с книги id=4 за полный тираж</h3>
    {{$authorFee}}
</body>
