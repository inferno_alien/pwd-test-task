<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Пользователь
 *
 * Class Author
 * @package App\Models
 *
 * @method static Builder byFIO(string $firstName, $secondName, $lastName)
 */
class Author extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'authors';

    /**
     * Отбор по ФИО автора
     *
     * @param Builder $query
     * @param string $firstName
     * @param string $lastName
     * @param string $secondName
     */
    public function scopeByFIO(Builder $query, string $firstName, string $secondName, string $lastName) {
        return $query->where('first_name', $firstName)
            ->where('last_name', $lastName)
            ->where('second_name', $secondName);
    }

    /**
     * The books that belong to the author.
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'authors_books');
    }
}
