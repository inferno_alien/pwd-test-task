<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'publishers';

    /**
     * Отбор по автору
     *
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeByTitle(Builder $query, string $title) {
        return $query->where('title', $title);
    }
}
