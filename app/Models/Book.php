<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App\Models
 *
 * @method static Builder byTitle(string $title)
 */
class Book extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * Отбор по названию книги
     *
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeByTitle(Builder $query, string $title) {
        return $query->where('title', $title);
    }

    /**
     * The authors that belong to the book.
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'authors_books');
    }

    public function publisher() {
        return $this->hasOne(Publisher::class);
    }
}
