<?php

namespace Database\Seeders;

use Database\Seeders\test\AuthorsBooksSeeder;
use Database\Seeders\test\AuthorsSeeder;
use Database\Seeders\test\BooksSeeder;
use Database\Seeders\test\PublishersSeeder;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Сидер для заполнения тестовых сидов
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthorsSeeder::class);
        $this->call(PublishersSeeder::class);
        $this->call(BooksSeeder::class);
        $this->call(AuthorsBooksSeeder::class);
    }
}
