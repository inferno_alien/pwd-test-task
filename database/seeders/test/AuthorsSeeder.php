<?php

namespace Database\Seeders\test;

use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $firstName = 'Александр';
        $secondName = 'Сергеевич';
        $lastName = 'Пушкин';

        $author = $this->findAuthor($firstName, $lastName, $secondName);

        if (!$author) {
            $model = new Author();

            $model->first_name = $firstName;
            $model->last_name = $lastName;
            $model->second_name = $secondName;
            $model->city = 'Москва';

            $model->save();
        }

        $firstName = 'Николай';
        $secondName = 'Васильевич';
        $lastName = 'Гоголь';

        $author = $this->findAuthor($firstName, $lastName, $secondName);

        if (!$author) {
            $model = new Author();

            $model->first_name = $firstName;
            $model->last_name = $lastName;
            $model->second_name = $secondName;
            $model->city = 'Санкт-Петербург';

            $model->save();
        }

        $firstName = 'Фёдор';
        $secondName = 'Михайлович';
        $lastName = 'Достоевский';

        $author = $this->findAuthor($firstName, $lastName, $secondName);

        if (!$author) {
            $model = new Author();

            $model->first_name = $firstName;
            $model->last_name = $lastName;
            $model->second_name = $secondName;
            $model->city = 'Москва';

            $model->save();
        }
    }

    private function findAuthor(string $firstName, string $lastName, string $secondName) {
        $author = Author::query()
            ->where('first_name', $firstName)
            ->where('last_name', $lastName)
            ->where('second_name', $secondName)
            ->first();

        return $author ?? null;
    }
}
