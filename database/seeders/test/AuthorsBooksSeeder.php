<?php

namespace Database\Seeders\test;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class AuthorsBooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = Author::byFIO('Александр', 'Сергеевич', 'Пушкин')->first();
        $book = Book::byTitle('Евгений Онегин')->first();

        if ($book) {
            $author->books()->syncWithoutDetaching($book);
        }

        $author = Author::byFIO('Фёдор', 'Михайлович', 'Достоевский')->first();
        $book = Book::byTitle('Преступление и наказание')->first();

        if ($book) {
            $author->books()->syncWithoutDetaching($book);
        }

        $author = Author::byFIO('Николай', 'Васильевич', 'Гоголь')->first();
        $book = Book::byTitle('Ревизор')->first();

        if ($book) {
            $author->books()->syncWithoutDetaching($book);
        }

        $author = Author::find(1);
        $authorB = Author::find(2);

        $book = Book::byTitle('Кроссовер')->first();

        if ($book) {
            $author->books()->syncWithoutDetaching($book);
            $authorB->books()->syncWithoutDetaching($book);
        }
    }
}
