<?php

namespace Database\Seeders\test;

use App\Models\Publisher;
use Illuminate\Database\Seeder;

class PublishersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = 'Эксмо';

        $publisher = $this->findPublisher($title);

        if (!$publisher) {
            $model = new Publisher();

            $model->title = $title;
            $model->city = 'Москва';
            $model->author_profit = 100;

            $model->save();
        }

        $title = 'Миф';

        $publisher = $this->findPublisher($title);

        if (!$publisher) {
            $model = new Publisher();

            $model->title = $title;
            $model->city = 'Санкт-Петербург';
            $model->author_profit = 150;

            $model->save();
        }

        $title = 'Лабиринт';

        $publisher = $this->findPublisher($title);

        if (!$publisher) {
            $model = new Publisher();

            $model->title = $title;
            $model->city = 'Москва';
            $model->author_profit = 80;

            $model->save();
        }
    }

    private function findPublisher(string $title) {
        $publisher = Publisher::query()
            ->where('title', $title)
            ->first();

        return $publisher ?? null;
    }
}
