<?php

namespace Database\Seeders\test;

use App\Models\Book;
use App\Models\Publisher;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = 'Евгений Онегин';

        $book = $this->findBook($title);

        if (!$book) {
            $model = new Book();

            $model->title = $title;
            $model->year = 1833;
            $model->copies_cnt = 2000000;
            $model->publisher_id = Publisher::find(1)->id;

            $model->save();
        }

        $title = 'Ревизор';

        $book = $this->findBook($title);

        if (!$book) {
            $model = new Book();

            $model->title = $title;
            $model->year = 1836;
            $model->copies_cnt = 3200000;
            $model->publisher_id = Publisher::find(2)->id;

            $model->save();
        }

        $title = 'Преступление и наказание';

        $book = $this->findBook($title);

        if (!$book) {
            $model = new Book();

            $model->title = $title;
            $model->year = 1865;
            $model->copies_cnt = 1800000;
            $model->publisher_id = Publisher::find(1)->id;

            $model->save();
        }

        $title = 'Кроссовер';

        $book = $this->findBook($title);

        if (!$book) {
            $model = new Book();

            $model->title = $title;
            $model->year = 1890;
            $model->copies_cnt = 7400000;
            $model->publisher_id = Publisher::find(1)->id;

            $model->save();
        }
    }

    private function findBook(string $title) {
        $book = Book::query()
            ->where('title', $title)
            ->first();

        return $book ?? null;
    }
}
